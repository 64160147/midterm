package com.supanut.midterm;

public class InvisibleWall extends Unit{
    public InvisibleWall(TheMaze TheMaze,int x,int y) {
        super(TheMaze,'-', x, y, true);
    }
}
