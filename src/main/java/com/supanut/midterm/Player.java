package com.supanut.midterm;

public class Player extends Unit{
    public Player(TheMaze TheMaze,char symbol,int x,int y){
        super(TheMaze, symbol, x, y, false);
    }

    public void up(){
        int y = this.getY();
        y--;
        this.setY(y);
    }
    public void down(){
        int y = this.getY();
        y++;
        this.setY(y);
    }
    public void left(){
        int x = this.getX();
        x--;
        this.setX(x);
    }
    public void right(){
        int x = this.getX();
        x++;
        this.setX(x);
    }
    public void setLocation1(int x,int y) {
        if(x == 4 && y == 1) {
        this.setX(11);
        this.setY(0);
        }
    }
    public void setLocation2(int x,int y) {
        if(x == 4 && y == 2) {
        this.setX(1);
        this.setY(8);
        }
    }
    public void setLocation3(int x,int y) {
        if(x == 3 && y == 8) {
        this.setX(9);
        this.setY(5);
        }
    }
    
}
