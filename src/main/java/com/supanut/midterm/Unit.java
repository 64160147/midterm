package com.supanut.midterm;

public class Unit {
    private char symbol;
    private int x;
    private int y;
    private TheMaze TheMaze;
    private boolean dominate;
    
    public Unit(TheMaze TheMaze,char symbol,int x,int y,boolean dominate){
        this.TheMaze = TheMaze;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.dominate = dominate;
    }
    public boolean isOn(int x,int y){
        return this.x == x && this.y == y;
    }
    public boolean setX(int x){
        if(!TheMaze.isOn(x, y)) return false;
        if(TheMaze.hasDominate(x, y)) return false;
        this.x = x;
        return true;
    }
    public boolean setY(int y){
        if(!TheMaze.isOn(x, y)) return false;
        if(TheMaze.hasDominate(x, y)) return false;
        this.y = y;
        return true;
    }
    public boolean setXY(int x,int y){
        if(!TheMaze.isOn(x, y)) return false;
        if(TheMaze.hasDominate(x, y)) return false;
        this.x = x;
        this.y = y;
        return true;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public boolean isDominate(){
        return dominate;
    }
    public TheMaze getTheMaze(){
        return TheMaze;
    }
    public char getSymbol(){
        return symbol;
    }
    public String toString(){
        return "Unit(" + this.symbol + ") [" + this.x + ", " + this.y + "] is on" + TheMaze;
    }
}
