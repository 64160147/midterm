package com.supanut.midterm;

import java.util.Scanner;

public class App {
    static TheMaze TheMaze = new TheMaze(13, 9);
    static Player player = new Player(TheMaze, 'P', 2, 4);
    static EndGame endGame = new EndGame(TheMaze, 11, 4);
    static int x = player.getX();
    static int y = player.getY();

    static TheWall TheWall1 = new TheWall(TheMaze, 5, 0);static TheWall TheWall2 = new TheWall(TheMaze, 10, 0);
    static TheWall TheWall3 = new TheWall(TheMaze, 12, 0);static TheWall TheWall4 = new TheWall(TheMaze, 1, 1);
    static TheWall TheWall5 = new TheWall(TheMaze, 3, 1);static TheWall TheWall6 = new TheWall(TheMaze, 5, 1);
    static TheWall TheWall7 = new TheWall(TheMaze, 6, 1);static TheWall TheWall8 = new TheWall(TheMaze, 7, 1);
    static TheWall TheWall9 = new TheWall(TheMaze, 8, 1);static TheWall TheWall10 = new TheWall(TheMaze, 10, 1);
    static TheWall TheWall11 = new TheWall(TheMaze, 12, 1);static TheWall TheWall12 = new TheWall(TheMaze, 1, 3);
    static TheWall TheWall13 = new TheWall(TheMaze, 3, 3);static TheWall TheWall14 = new TheWall(TheMaze, 1, 3);
    static TheWall TheWall15 = new TheWall(TheMaze, 3, 3);static TheWall TheWall16 = new TheWall(TheMaze, 4, 3);
    static TheWall TheWall17 = new TheWall(TheMaze, 6, 3);static TheWall TheWall18 = new TheWall(TheMaze, 8, 3);
    static TheWall TheWall19 = new TheWall(TheMaze, 10, 3);static TheWall TheWall20 = new TheWall(TheMaze, 6, 4);
    static TheWall TheWall21 = new TheWall(TheMaze, 1, 5);static TheWall TheWall22 = new TheWall(TheMaze, 3, 5);
    static TheWall TheWall23 = new TheWall(TheMaze, 4, 5);static TheWall TheWall24 = new TheWall(TheMaze, 6, 5);
    static TheWall TheWall25 = new TheWall(TheMaze, 8, 5);static TheWall TheWall26 = new TheWall(TheMaze, 10, 5);
    static TheWall TheWall27 = new TheWall(TheMaze, 11, 5);static TheWall TheWall28 = new TheWall(TheMaze, 1, 6);
    static TheWall TheWall29 = new TheWall(TheMaze, 3, 6);static TheWall TheWall30 = new TheWall(TheMaze, 9, 6);
    static TheWall TheWall31 = new TheWall(TheMaze, 10, 6);static TheWall TheWall32 = new TheWall(TheMaze, 1, 7);
    static TheWall TheWall33 = new TheWall(TheMaze, 2, 7);static TheWall TheWall34 = new TheWall(TheMaze, 3, 7);
    static TheWall TheWall35 = new TheWall(TheMaze, 5, 7);static TheWall TheWall36 = new TheWall(TheMaze, 6, 7);
    static TheWall TheWall37 = new TheWall(TheMaze, 8, 7);static TheWall TheWall38 = new TheWall(TheMaze, 10, 7);
    static TheWall TheWall39 = new TheWall(TheMaze, 11, 7);

    static InvisibleWall InvisibleWall1 = new InvisibleWall(TheMaze, 2, 2);
    static InvisibleWall InvisibleWall2 = new InvisibleWall(TheMaze, 6, 2);
    static InvisibleWall InvisibleWall3 = new InvisibleWall(TheMaze, 9, 3);
    static InvisibleWall InvisibleWall4 = new InvisibleWall(TheMaze, 11, 3);
    static InvisibleWall InvisibleWall5 = new InvisibleWall(TheMaze, 4, 4);
    static InvisibleWall InvisibleWall6 = new InvisibleWall(TheMaze, 10, 4);
    static InvisibleWall InvisibleWall7 = new InvisibleWall(TheMaze, 4, 7);
    static InvisibleWall InvisibleWall8 = new InvisibleWall(TheMaze, 2, 8);

    static MiracleWall MiracleWall1 = new MiracleWall(TheMaze, 12, 3);static MiracleWall MiracleWall2 = new MiracleWall(TheMaze, 8, 4);

    static Teleport Teleport1 = new Teleport(TheMaze, 4, 1);
    static Teleport Teleport2 = new Teleport(TheMaze, 3, 8);
    static Teleport Teleport3 = new Teleport(TheMaze, 4, 2);

    static int i = 0;
    static int j = 0;
    static int n = 1;
    static Data data1 = new Data("Move  :", i);
    static Data data2 = new Data("score :", 10000);
    static Data data3 = new Data("Rank  :", 0);

    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process2(String command) {
        switch (command) {
            case "w":
                i++;
                j+=100;
                y--;
                player.up();
                break;
            case "s":
                i++;
                j+=100;
                y++;
                player.down();
                break;
            case "a":
                i++;
                j+=100;
                x--;
                player.left();
                break;
            case "d":
                i++;
                j+=100;
                x++;
                player.right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        TheMaze.add(TheWall1);TheMaze.add(TheWall2);TheMaze.add(TheWall3);TheMaze.add(TheWall4);TheMaze.add(TheWall5);
        TheMaze.add(TheWall6);TheMaze.add(TheWall7);TheMaze.add(TheWall8);TheMaze.add(TheWall9);TheMaze.add(TheWall10);
        TheMaze.add(TheWall11);TheMaze.add(TheWall12);TheMaze.add(TheWall13);TheMaze.add(TheWall14);TheMaze.add(TheWall15);
        TheMaze.add(TheWall16);TheMaze.add(TheWall17);TheMaze.add(TheWall18);TheMaze.add(TheWall19);TheMaze.add(TheWall20);
        TheMaze.add(TheWall21);TheMaze.add(TheWall22);TheMaze.add(TheWall23);TheMaze.add(TheWall24);TheMaze.add(TheWall25);
        TheMaze.add(TheWall26);TheMaze.add(TheWall27);TheMaze.add(TheWall28);TheMaze.add(TheWall29);TheMaze.add(TheWall30);
        TheMaze.add(TheWall31);TheMaze.add(TheWall32);TheMaze.add(TheWall33);TheMaze.add(TheWall34);TheMaze.add(TheWall35);
        TheMaze.add(TheWall36);TheMaze.add(TheWall37);TheMaze.add(TheWall38);TheMaze.add(TheWall39);

        TheMaze.add(InvisibleWall1);TheMaze.add(InvisibleWall2);TheMaze.add(InvisibleWall3);TheMaze.add(InvisibleWall4);
        TheMaze.add(InvisibleWall5);TheMaze.add(InvisibleWall6);TheMaze.add(InvisibleWall7);TheMaze.add(InvisibleWall8);

        TheMaze.add(MiracleWall1);TheMaze.add(MiracleWall2);

        TheMaze.add(Teleport1);TheMaze.add(Teleport2);TheMaze.add(Teleport3);

        TheMaze.add(player);TheMaze.add(endGame);

        System.out.println("=============");
        System.out.println("             ");
        System.out.println("W E L C O M E");
        System.out.println("     T O     ");
        System.out.println("    T H E    ");
        System.out.println("   M A Z E   ");
        System.out.println("             ");
        System.out.println("=============");
        System.out.println(" w To Up     ");
        System.out.println(" s To Down   ");
        System.out.println(" a To Left   ");
        System.out.println(" d To Right  ");
        System.out.println(" q To Exit   ");
        System.out.println("=============");
        System.out.println();

        while (true) {
            System.out.println("   #     #   ");
            System.out.println("  # #   # #  ");
            System.out.println(" #   # #   # ");
            System.out.println(" #    #    # ");
            System.out.println(" #    #    # ");
            System.out.println(" #    #    # ");
            System.out.println("=============");
            System.out.println("=  TheMAZE  =");
            System.out.println("=============");

            if (x == 11 && y == 4) {
                data1.DataIncrease(i);
                data2.DataCourse(j);
                if(i==17) {
                    int s = 1;
                    data3.DataRanking(n, s);
                }
                if(i > 17 && i <= 30) {
                    int s = 2;
                    data3.DataRanking(n, s);
                }
                if(i > 30 && i <= 50) {
                    int s = 3;
                    data3.DataRanking(n, s);
                }
                if(i > 50 && i <= 100) {
                    int s = 4;
                    data3.DataRanking(n, s);
                }
                if(i > 100) {
                    int s = 0;
                    data3.DataRanking(n, s);
                }
                data1.print();
                data2.print();
                data3.print();
                System.out.println("=============");
                System.out.println("=   E N D   =");
                System.out.println("=============");
                System.out.println("  T H A N K  ");
                System.out.println("    F O R    ");
                System.out.println("P L A Y I N G");
                System.out.println("=============");
                endGame.end(11, 4);
            }
            if (x == 4 && y == 1) {
                x = 11;
                y = 0;
                player.setLocation1(4, 1);
            }
            if (x == 4 && y == 2) {
                x = 1;
                y = 8;
                player.setLocation2(4, 2);
            }
            if (x == 3 && y == 8) {
                x = 9;
                y = 5;
                player.setLocation3(3, 8);
            }
            TheMaze.print();
            System.out.println("=============");
            System.out.println();
            System.out.print("order : ");
            String command = input();
            process2(command);
            System.out.println();
            System.out.println("=============");
        }
    }
}
