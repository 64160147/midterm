package com.supanut.midterm;

public class Data {
    private String name;
    private int numberData;
    public Data(String name,int numberData) {
        this.name = name;
        this.numberData = numberData;
    }

    public boolean DataIncrease(int n) {
        numberData = numberData + n;
        return true;
    }

    public boolean DataCourse(int n) {
        numberData = numberData - n;
        return true;
    }
    public boolean DataRanking(int n,int s) {
        numberData = n+s;
        return true;
    }

    public void print() {
        System.out.println(name+" "+numberData);
    }

    public String getName() {
        return name;
    }
    public int getnumberData() {
        return numberData;
    }
}

