package com.supanut.midterm;

public class TheMaze {
    private int width;
    private int height;
    private Unit units[];
    private int unitCont;
    public TheMaze(int width,int height){
        this.width = width;
        this.height = height;
        this.units = new Unit[width*height];
        this.unitCont = 0;
    }
    public TheMaze(){
        this(20,20);
    }
    public void print(){
        for(int y = 0; y < this.height; y++){
            for(int x = 0; x < this.width; x++){
                printBlock(x, y);
            }
            System.out.println();
        }
    }
    private void printBlock(int x,int y){
        for(int i = 0  ; i < unitCont ; i++){
            Unit unit = this.units[i];
            if(unit.isOn(x, y)){
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print("-");
    }
    public String toString(){
        return "TheMaze(" + this.width + "," + this.height + ")";
    }
    public void add(Unit unit){
        if(unitCont == (width*height)) return;
        this.units[unitCont] = unit;
        unitCont++;
    }
    public void printUnits(){
        for(int i = 0; i < unitCont ; i++){
            System.out.println(this.units[i]);
        }
    }
    public boolean isOn(int x,int y){
        return isInWidth(x) && isInHeight(y);
    }
    public boolean isInWidth(int x){
        return x >= 0 && x < width;
    }
    public boolean isInHeight(int y){
        return y >= 0 && y < height; 
    }
    public boolean hasDominate(int x,int y){
        for(int i = 0; i < unitCont; i++){
            Unit unit = this.units[i];
            if(unit.isDominate() && unit.isOn(x, y)){
                return true;
            }
        }
        return false;
    }

    
}
    